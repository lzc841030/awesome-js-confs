import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import AuthPlugin from './plugins/auth';
import router from './router';
import { createProvider } from './vue-apollo';

Vue.config.productionTip = false;

Vue.use(AuthPlugin);

new Vue({
  router,
  apolloProvider: createProvider(),
  render: h => h(App),
}).$mount('#app');
